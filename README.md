# README #

Nur ein einfacher Bot. Skye spricht fast durchgehend deutsch.

### Features ###

* Auf "Hallo" Antworten und diverse einfache Kommandos für den BotOwner
* Musik abspielen aus einem Verzeichnis des ausführenden PCs

### Setup ###

* Projekt in Visual Studio öffnen und kompilieren
* Die Beispiel-Konfigurationsdatei aus `./files/config` in den Unterordner -> `./SkyeBot/bin/Debug/config/global.json` kopieren
* In der `global.json` das Bot-Token und deine UserID (BotOwner) eintragen. Für das Bot-Token musst du unter https://discordapp.com/developers/applications/me eine App erstellen und den "Bot User" aktivieren. Für die User-ID schreibe in irgendeinen Discord-Chat `\@DEINNAME-1234`
* Starten

### Hilfe ###

* --> [Issues](https://bitbucket.org/larsgr/skye-bot/issues)