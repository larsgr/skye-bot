﻿using System;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace SkyeBot
{
    public class GlobalSettings
    {
        private const string Path = "./config/global.json";
        private static GlobalSettings _instance = new GlobalSettings();

        public static void Load()
        {
            if (!File.Exists(Path))
                throw new FileNotFoundException($"{Path} is missing.");
            _instance = JsonConvert.DeserializeObject<GlobalSettings>(File.ReadAllText(Path));

        }
        public static void Save()
        {
            using (var stream = new FileStream(Path, FileMode.Create, FileAccess.Write, FileShare.None))
            using (var writer = new StreamWriter(stream))
                writer.Write(JsonConvert.SerializeObject(_instance, Formatting.Indented));
        }

        //Discord
        public class DiscordSettings
        {
            [JsonProperty("token")]
            public string Token;

            [JsonProperty("botowner")]
            public ulong BotOwner;
        }

        [JsonProperty("discord")]
        private readonly DiscordSettings _discord = new DiscordSettings();
        public static DiscordSettings Discord => _instance._discord;

    }
}

