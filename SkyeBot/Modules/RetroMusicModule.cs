﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Discord;
using Discord.Audio;
using Discord.Commands;
using Discord.Commands.Permissions.Visibility;
using Discord.Modules;
using NAudio.Wave;

namespace SkyeBot.Modules
{
    class RetroMusicModule : IModule
    {
        private ModuleManager _manager;
        private DiscordClient _client;

        private bool _cancelPlayback = false;
        private Channel _currentlyPlaying = null;

        public void Install(ModuleManager manager)
        {
            _manager = manager;
            _client = manager.Client;

            _manager.CreateCommands("", b =>
            {
                b.PublicOnly();
                b.CreateCommand("play")
                    .Alias("songs", "song")
                    .Description("Ich kann dir ein paar Lieder spielen. Schreibe `!play` für eine Liste")
                    .Parameter("song", ParameterType.Optional)
                    .Do(async e =>
                    {
                        if (e.Args[0] == "help" || e.Args[0] == "list" || e.Args[0] == "")
                        {
                            string list = "";
                            foreach (string file in Directory.GetFiles(Path.Combine(Environment.CurrentDirectory, "songs"), "*.mp3"))
                            {
                                FileInfo fi = new FileInfo(file);
                                list += "\n" + fi.Name.Replace(".mp3", "");
                            }

                            await e.Channel.SendMessage("**Songs verfügbar:** " + list);
                            return;
                        }

                        if (!File.Exists(Path.Combine(Environment.CurrentDirectory, "songs", e.Args[0] + ".mp3")))
                            await e.Channel.SendMessage("Das Lied kenne ich nicht. :confused: ");
                        else
                            await PlaySong(e, "songs/"+e.Args[0]+".mp3");
                    });
                b.CreateCommand("stop")
                    .Alias("abort", "abortplay")
                    .Description("Stoppt alle Wiedergaben")
                    .Do(e =>
                    {
                        _client.Log.Info("RetroMusicModule", $"User {e.User.Name} aborted the playback in {e.User.VoiceChannel.Name}");
                        _cancelPlayback = true;
                    });
            });

        }

        private async Task PlaySong(CommandEventArgs e, string filePath)
        {
            Channel channel = e.User.VoiceChannel;
            if (channel == null)
            {
                await e.Channel.SendMessage("Du bist in keinem Voice-Channel. :stuck_out_tongue_closed_eyes: ");
                return;
            }

            if (_currentlyPlaying == channel)
            {
                // Already playing here
                await e.Channel.SendMessage("Ich spiele doch schon :stuck_out_tongue_closed_eyes: ");
                return;
            }

            if (_currentlyPlaying != null)
            {
                // Abort playback in other channels
                _cancelPlayback = true;
                while (_currentlyPlaying != null)
                {
                    Thread.Sleep(200);
                }
                Thread.Sleep(1000);
            }
            else
            {
                _cancelPlayback = false;
            }

            // Start here
            _currentlyPlaying = channel;

            IAudioClient vclient = await _client.GetService<AudioService>().Join(channel);

            SendAudio(vclient, filePath);

            //await vclient.Disconnect();

            _currentlyPlaying = null;
        }

        private void SendAudio(IAudioClient client, string filePath)
        {
            var channelCount = _client.GetService<AudioService>().Config.Channels;
            var outFormat = new WaveFormat(48000, 16, channelCount);
            using (var mp3Reader = new Mp3FileReader(filePath))
            using (var resampler = new MediaFoundationResampler(mp3Reader, outFormat))
            {
                resampler.ResamplerQuality = 60;
                int blockSize = outFormat.AverageBytesPerSecond / 50;
                byte[] buffer = new byte[blockSize];
                int byteCount;

                while ((byteCount = resampler.Read(buffer, 0, blockSize)) > 0)
                {
                    if (_cancelPlayback)
                    {
                        _cancelPlayback = false;
                        _client.Log.Info("RetroMusicModule", "Playback stopped");
                        break;
                    }

                    if (byteCount < blockSize)
                    {
                        for (int i = byteCount; i < blockSize; i++)
                            buffer[i] = 0;
                    }
                    client.Send(buffer, 0, blockSize);
                }
            }
        }

    }
}
