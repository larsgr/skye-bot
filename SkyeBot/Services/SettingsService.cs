﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Modules;
using Newtonsoft.Json;

namespace SkyeBot.Services
{
    public class SettingsManager<TGlobalSettings, TSettings> : SettingsManager<TSettings>
        where TGlobalSettings : class, new()
        where TSettings : class, new()
    {
        public TGlobalSettings Global { get; private set; }

        public SettingsManager(string name) : base(name)
        {
        }

        public override void LoadConfigs()
        {
            var path = $"{Directory}/global.json";
            if (File.Exists(path))
                Global = JsonConvert.DeserializeObject<TGlobalSettings>(File.ReadAllText(path));
            else
                Global = new TGlobalSettings();

            base.LoadConfigs();
        }

        public async Task SaveGlobal(TGlobalSettings settings)
        {
            while (true)
            {
                try
                {
                    using (var fs = new FileStream($"{Directory}/global.json", FileMode.Create, FileAccess.Write, FileShare.None))
                    using (var writer = new StreamWriter(fs))
                        await writer.WriteAsync(JsonConvert.SerializeObject(Global));
                    break;
                }
                catch (IOException) //In use
                {
                    await Task.Delay(1000);
                }
            }
        }
    }

    public class SettingsManager<TSettings> where TSettings : class, new()
    {
        protected readonly string Directory;

        public IEnumerable<KeyValuePair<ulong, TSettings>> AllServers => _servers;
        private ConcurrentDictionary<ulong, TSettings> _servers;

        public SettingsManager(string name)
        {
            Directory = $"./config/{name}";
            System.IO.Directory.CreateDirectory(Directory);

            LoadConfigs();
        }

        public bool RemoveServer(ulong id)
        {
            TSettings settings;
            if (_servers.TryRemove(id, out settings))
            {
                var path = $"{Directory}/{id}.json";
                if (File.Exists(path))
                    File.Delete(path);
                return true;
            }
            return false;
        }

        public virtual void LoadConfigs()
        {
            var servers = System.IO.Directory.GetFiles(Directory)
                .Select(x =>
                {
                    ulong id;
                    if (ulong.TryParse(Path.GetFileNameWithoutExtension(x), out id))
                        return id;
                    else
                        return (ulong?)null;
                })
                .Where(x => x.HasValue)
                .ToDictionary(x => x.Value, x =>
                {
                    string path = $"{Directory}/{x}.json";
                    if (File.Exists(path))
                        return JsonConvert.DeserializeObject<TSettings>(File.ReadAllText(path));
                    else
                        return new TSettings();
                });

            _servers = new ConcurrentDictionary<ulong, TSettings>(servers);
        }

        public TSettings Load(Server server)
            => Load(server.Id);
        public TSettings Load(ulong serverId)
        {
            TSettings result;
            if (_servers.TryGetValue(serverId, out result))
                return result;
            else
                return new TSettings();
        }

        public Task Save(Server server, TSettings settings)
            => Save(server.Id, settings);
        public Task Save(KeyValuePair<ulong, TSettings> pair)
            => Save(pair.Key, pair.Value);
        public async Task Save(ulong serverId, TSettings settings)
        {
            _servers[serverId] = settings;

            while (true)
            {
                try
                {
                    using (var fs = new FileStream($"{Directory}/{serverId}.json", FileMode.Create, FileAccess.Write, FileShare.None))
                    using (var writer = new StreamWriter(fs))
                        await writer.WriteAsync(JsonConvert.SerializeObject(settings));
                    break;
                }
                catch (IOException) //In use
                {
                    await Task.Delay(1000);
                }
            }
        }
    }

    public class SettingsService : IService
    {
        public void Install(DiscordClient client) { }

        public SettingsManager<TSettings> AddModule<TModule, TSettings>(ModuleManager manager)
            where TSettings : class, new()
            => new SettingsManager<TSettings>(manager.Id);

        public SettingsManager<TGlobalSettings, TSettings> AddModule<TModule, TGlobalSettings, TSettings>(ModuleManager manager)
            where TGlobalSettings : class, new()
            where TSettings : class, new()
            => new SettingsManager<TGlobalSettings, TSettings>(manager.Id);
    }
}
