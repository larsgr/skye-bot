﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.Modules;
using System.IO;
using System.Net;
using Discord.Audio;
using Discord.Commands.Permissions.Levels;
using NAudio.Wave;
using SkyeBot.Modules;
using SkyeBot.Services;

// ReSharper disable LocalizableElement

namespace SkyeBot
{
    class Program
    {
        static void Main() => new Program().Start();

        internal string AppName = "Skye";
        internal string AppUrl = "lars.systems";

        private DiscordClient _client;
        
        public void Start()
        {
#if !DNXCORE50
            Console.Title = $"{AppName} (Discord.Net v{DiscordConfig.LibVersion})";
#endif
            GlobalSettings.Load();

            _client = new DiscordClient(x =>
                {
                    x.AppName = AppName;
                    x.AppUrl = AppUrl;
                    x.MessageCacheSize = 0;
                    x.UsePermissionsCache = false;
                    x.EnablePreUpdateEvents = true;
                    x.LogLevel = LogSeverity.Info;
                    x.LogHandler = OnLogMessage;
                })
                .UsingCommands(x =>
                {
                    x.AllowMentionPrefix = true;
                    x.HelpMode = HelpMode.Public;
                    x.ExecuteHandler = OnCommandExecuted;
                    x.ErrorHandler = OnCommandError;
                    x.PrefixChar = '!';
                })
                .UsingModules()
                .UsingAudio(x =>
                {
                    x.Mode = AudioMode.Outgoing;
                    x.EnableEncryption = true;
                    x.Bitrate = AudioServiceConfig.MaxBitrate;
                    x.BufferLength = 10000;
                })
                .UsingPermissionLevels(PermissionResolver);

            _client.AddService<SettingsService>();
            _client.AddService<HttpService>();

            _client.AddModule<RetroMusicModule>("Retro Music");
            _client.AddModule<TwitchModule>("Twitch");
            _client.AddModule<PublicModule>("");
            _client.AddModule<YoutubeModule>("YouTube");
            _client.AddModule<GooglePlayMusicModule>("GooglePlayMusic");

            _client.ServerAvailable += delegate
            {
                Console.WriteLine("Server available: "+_client.Servers.Count());

                foreach (Server server in _client.Servers)
                    Console.WriteLine("Found server: " + server.Name);

                //_client.CurrentUser.Edit("", null, null, null, File.OpenRead("C:\\Users\\larsg\\Desktop\\skye-small.jpg"), ImageType.Jpeg);

            };

            _client.ExecuteAndWait(async () => {
                await _client.Connect(GlobalSettings.Discord.Token, TokenType.Bot);
            });

        }



        private void OnCommandError(object sender, CommandErrorEventArgs e)
        {
            string msg = e.Exception?.Message;
            if (msg == null) //No exception - show a generic message
            {
                switch (e.ErrorType)
                {
                    case CommandErrorType.Exception:
                        msg = "Unknown error.";
                        break;
                    case CommandErrorType.BadPermissions:
                        msg = "You do not have permission to run this command.";
                        break;
                    case CommandErrorType.BadArgCount:
                        msg = "You provided the incorrect number of arguments for this command.";
                        break;
                    case CommandErrorType.InvalidInput:
                        msg = "Unable to parse your command, please check your input.";
                        break;
                    case CommandErrorType.UnknownCommand:
                        msg = "Unknown command.";
                        break;
                }
            }
            if (msg != null)
            {
                //_client.ReplyError(e, msg);
                _client.Log.Error("Command", msg);
            }
        }
        private void OnCommandExecuted(object sender, CommandEventArgs e)
        {
            _client.Log.Info("Command", $"{e.Command.Text} ({e.User.Name})");
        }
        private void OnLogMessage(object sender, LogMessageEventArgs e)
        {
            //Color
            ConsoleColor color;
            switch (e.Severity)
            {
                case LogSeverity.Error: color = ConsoleColor.Red; break;
                case LogSeverity.Warning: color = ConsoleColor.Yellow; break;
                case LogSeverity.Info: color = ConsoleColor.White; break;
                case LogSeverity.Verbose: color = ConsoleColor.Gray; break;
                case LogSeverity.Debug: default: color = ConsoleColor.DarkGray; break;
            }

            //Exception
            string exMessage;
            Exception ex = e.Exception;
            if (ex != null)
            {
                while (ex is AggregateException && ex.InnerException != null)
                    ex = ex.InnerException;
                exMessage = ex.Message;
            }
            else
                exMessage = null;

            //Source
            string sourceName = e.Source?.ToString();

            //Text
            string text;
            if (e.Message == null)
            {
                text = exMessage ?? "";
                exMessage = null;
            }
            else
                text = e.Message;

            //Build message
            StringBuilder builder = new StringBuilder(text.Length + (sourceName?.Length ?? 0) + (exMessage?.Length ?? 0) + 5);
            if (sourceName != null)
            {
                builder.Append('[');
                builder.Append(sourceName);
                builder.Append("] ");
            }
            for (int i = 0; i < text.Length; i++)
            {
                //Strip control chars
                char c = text[i];
                if (!char.IsControl(c))
                    builder.Append(c);
            }
            if (exMessage != null)
            {
                builder.Append(": ");
                builder.Append(exMessage);
            }

            text = builder.ToString();
            Console.ForegroundColor = color;
            Console.WriteLine(text);
        }
        private int PermissionResolver(User user, Channel channel)
        {
            if (user.Id == GlobalSettings.Discord.BotOwner)
                return (int)PermissionLevel.BotOwner;
            if (user.Server != null)
            {
                if (user == channel.Server.Owner)
                    return (int)PermissionLevel.ServerOwner;

                var serverPerms = user.ServerPermissions;
                if (serverPerms.ManageRoles)
                    return (int)PermissionLevel.ServerAdmin;
                if (serverPerms.ManageMessages && serverPerms.KickMembers && serverPerms.BanMembers)
                    return (int)PermissionLevel.ServerModerator;

                var channelPerms = user.GetPermissions(channel);
                if (channelPerms.ManagePermissions)
                    return (int)PermissionLevel.ChannelAdmin;
                if (channelPerms.ManageMessages)
                    return (int)PermissionLevel.ChannelModerator;
            }
            return (int)PermissionLevel.User;
        }

        
    }
}
